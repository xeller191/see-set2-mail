const {createServer} = require('http')
require('dotenv').config()
const express = require('express')
const compression = require('compression')
const morgan = require('morgan')
const path = require('path')
const normalizePort = port =>parseInt(port, 10)
const PORT = normalizePort(process.env.PORT || 5000)
const nodemailer = require("nodemailer");
var bodyParser = require('body-parser');





const app = express()
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json())

const dev = app.get('env') !== 'production'
app.post('/mail/',(req,res)=>{
    // console.log("sent mail 1");
    const {email , name , detail} =req.body
    // console.log(req.body)
    let transporter = nodemailer.createTransport({  
        service:'gmail',
        auth: {
            user: process.env.MY_EMAIL,
            pass: process.env.PASSWORD
        }
    });
    
    let mailOptions = {
        from: 'zenalyse.dev2021@gmail.com',
        to: 'zenalyse.dev2021@gmail.com',
        subject: 'Interested SEESET',
        text: `email from : ${email} , name : ${name} , detail : ${detail}`
    };
    
    transporter.sendMail(mailOptions, (error, info) => {
        // console.log("sentmai 2");
        if (error) {
            return console.log("error is 55 =>",error);
        }
        res.json({status:"sent success"})
        // console.log('success');
    });

})


if(!dev){
    app.disable('x-powerd-by')
    app.use(compression())
    app.use(morgan('common'))
    app.use(express.static(path.resolve(__dirname,'build')))
    app.get('*',(req,res)=>{
        res.sendFile(path.resolve(__dirname,'build','index.html'))
    })


if(dev){
    app.use(morgan('dev'))
}
}
const server = createServer(app)

server.listen(PORT, err=>{
    if(err) throw err;
    console.log(`server start on ${PORT}`);
})