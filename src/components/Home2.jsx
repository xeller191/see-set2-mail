import React, { useEffect, useState } from "react";

import Header from "./Header";
import '../assets/css/home.css'
import {Fade} from 'react-reveal';
import Sticky from 'react-stickynode';
import videoSrc from "../assets/img/home/Move_Layout.mp4";
import Layout_info from "../assets/img/home/Layout_info.png";
import ListStatus from "../assets/img/home/ListStatus.png";
import ListProgress from "../assets/img/home/ListProgress.png";
import ListOEE from "../assets/img/home/ListOEE.png";
import Move_Timeline from "../assets/img/home/Move_Timeline.mp4";
import Report_Losstime_by_Reason from "../assets/img/home/Report_Losstime_by_Reason.png";
import Report_Daily from "../assets/img/home/Report_Daily.png";
import Towerlight_text from "../assets/img/home/Towerlight_text.png";
import Install_Towerlight from "../assets/img/home/Install_Towerlight.mp4";
import Server_aws from "../assets/img/home/Server_aws.png";
import Server_OnPermise from "../assets/img/home/Server_OnPermise.png";
import MockupCardView from "../assets/img/home/MockupCardView.png";
import factory from "../assets/img/home/Factory.png";
import HambergaerMenu from "./HambergaerMenu";
import $ from 'jquery';
const Home2 = () => {
  const [safari, setSafari] = useState(false)
  const [hamshow , setHamshow ] = useState(false)
  useEffect(()=>{
    if(hamshow){
      function name() {
        $('.home2__cotainer__scroll').addClass('home2__cotainer__scroll2').removeClass('home2__cotainer__scroll');
      }
      name()
    }else{
      function name() {
        $('.home2__cotainer__scroll2').addClass('home2__cotainer__scroll').removeClass('home2__cotainer__scroll2');
      }
      name()
    }
  },[hamshow])
  //mute or unmute for ios only
  const [isVideoMuted1, setIsVideoMuted1] = useState(true)
  const [isVideoMuted2, setIsVideoMuted2] = useState(true)
  const [isVideoMuted3, setIsVideoMuted3] = useState(true)
 

   // run this function from an event handler or an effect to execute scroll 
  const handleMuteState1 = () => {
    setIsVideoMuted1(!isVideoMuted1)
  }
  const handleMuteState2 = () => {
    setIsVideoMuted2(!isVideoMuted2)
  }
  const handleMuteState3 = () => {
    setIsVideoMuted3(!isVideoMuted3)
  }
  return (
    <div className="container_home">
        <div className="container__header2" >
          <Header />
          <HambergaerMenu togglehamberger={setHamshow}/>
        </div>
        <div className="home2__cotainer__scroll">
          <Sticky top={1} enabled={true}>
          <Fade bottom  duration={2500}>
            <div className="machineLayout__container animated animatedFadeInUp fadeInUp" >
              <img className="machineLayout_image" src={MockupCardView} alt=""/>
              <div className="machineLayout__text-container">   
                  <h1>SEESET</h1>
                  <h1>Industrial IOT 4.0</h1>
                  <h1>Real-time machine monitoring solution</h1>
              </div>
            </div>
          </Fade>
          </Sticky>

          <Sticky top={2} enabled={true}>
          <div className="factoryLayout__container"  >
            <Fade left  duration={2500} >
              <div className="factoryLayout__container-text">
                <h1 className="factoryLayout__text">ย่อทั้งโรงงานให้อยู่</h1>
                <h1 className="factoryLayout__text" >ในมือคุณ</h1>
              </div>
            </Fade>
            <Fade right duration={2500} >
              <img className="factoryLayout__factory" src={factory} alt=""/>
            </Fade>
          </div>
          </Sticky>

          <Sticky top={3}  enabled={false}>
            <div className="video-machine-layout__container" >
              <Fade left  duration={2500} >
              <div  className="wrapText" >
                <h1 className="video-machine-layout__font">รู้ทุกตำแหน่งของเครื่องจักร พร้อมสถานะแบบ Real-Time</h1>
              </div>
              </Fade>
              <video className="video-machine-layout__video" autoPlay={true} loop={true} muted={isVideoMuted1} src={videoSrc} />
              {/* {safari && <button onClick={handleMuteState1}>{isVideoMuted1 ? 'Play' : 'Puase'}</button >} */}
            </div>
          </Sticky>

          <Sticky enabled={true}>
            <div className="report1__container" >
              <Fade left duration={2500} >
                <h1 className="report1__text">เจาะลึกความเคลื่อนไหวของเครื่องจักร</h1>
              </Fade>
              <Fade right  duration={2500} >
                <img className="report1__image" src={Layout_info} alt=""/>
              </Fade>
            </div>
          </Sticky>

          <Sticky top={5} enabled={true}>
            <div className="container_5" >
              <div className="dummy"/>
              <Fade left duration={2500} >
                  <h1 className="container_5__text ">บันทึกทุกสถานะที่เกิดขึ้นตั้งแต่เริ่มทำงาน</h1>
                  <h1 className="container_5__text text2">พร้อมสรุปเวลาที่เครื่องจักรทำงานจริง</h1>
              </Fade>
              <Fade right duration={2500} >
                <img className="container_5_picture" src={ListStatus} alt=""/>
              </Fade>
            </div>
          </Sticky>

          <Sticky top={6} enabled={true}>
            <div className="container_5 " >
              <Fade left duration={2500} >
                <div className="dummy" />
                <h1 className="container_5__text">ติดตามความคืบหน้าของการผลิตอย่างใกล้ชิด</h1>
              </Fade>
              <Fade right duration={2500} >
                <img className="container_5_picture" src={ListProgress} alt=""/>
              </Fade>
            </div>
          </Sticky>

          <Sticky top={7} enabled={true}>
            <div className="container_5 ">
              <Fade left  duration={2500} >
                <div className="dummy" />
                <h1 className="container_5__text">สรุปประสิทธิภาพการทำงานโดยรวมของเครื่องจักร(OEE)</h1>
              </Fade>
              <Fade right duration={2500} >
                <img className="container_5_picture"  src={ListOEE} alt=""/>
              </Fade>
            </div>
          </Sticky>


          <Sticky top={8} enabled={true}>
            <div className="container_6">
              <Fade left  duration={2500} >
                <div className="dummy6" />
                <h1 className="container_6__text">เช็กสถานการณ์ของเครื่องจักรที่ยุ่งยากได้อย่างง่ายดายจากข้อมูลที่น่าเชื่อถือ</h1>
                <div className="dummy6" />

              </Fade>
              <video className="video-machine-layout__video" autoPlay={true} loop={true} muted={true} src={Move_Timeline} />

            </div>
          </Sticky>

          <Sticky top={9} enabled={true}>
            <div className="container_7 "  >
              <Fade left  duration={2500} >
                <div className="dummy" />
                <h1 className="container_7__text">แหล่งรวมที่จะทำให้คุณรู้ถึงสาเหตุที่เครื่องจักรหยุดทำงาน</h1>
              </Fade>
              <div className="dummy5" />
              <Fade right  duration={2500} >
                <img className="container_7__picture" src={Report_Losstime_by_Reason} alt=""/>
              </Fade>
            </div>
          </Sticky>

          <Sticky top={10} enabled={true}>
            <div className="container_8" >
              <Fade up  duration={2500} >
                <div className="dummy" />
                <h1 className="container_8__text-header">ดูสรุปรายงานข้อมูลที่เกิดขึ้นกับเครื่องจักรได้ถึง 5 แบบ</h1>
              </Fade>
              <div className="container__wrap">
                <Fade left  duration={2500} >
                  <div className="dummy" />
                  <div className="container_8_picture">
                    <img className="container_8__img-show" src={Report_Daily} alt=""/>
                  </div>
                </Fade>
                <Fade right  duration={2500} >
                  <div className="container_text">
                    <h1 className="container_8__text-topic">Production report</h1>
                    <h1 className="container_8__text-detail">รายงานผลการผลิต</h1>
                    <h1 className="container_8__text-topic">Status report</h1>
                    <h1 className="container_8__text-detail">รายงานสถานะที่ส่งมาจากเครื่องจักรทั้งหมด</h1>
                    <h1 className="container_8__text-topic">Loss Time</h1>
                    <h1 className="container_8__text-detail">รายงานสาเหตุที่ทำให้เครื่องหยุดทำงาน พร้อมสรุปจำนวนครั้ง และระยะเวลาที่เกิดขึ้น</h1>
                    <h1 className="container_8__text-topic">Loss Time by Machine</h1>
                    <h1 className="container_8__text-detail">รายงานสถิติระยะเวลารูปแบบกราฟ ที่เครื่องจักรจักรหยุดทำงานแต่เครื่อง พร้อมสาเหตุที่ทำให้เครื่องหยุด และระยะเวลาของแต่ละสาเหตุ</h1>
                    <h1 className="container_8__text-topic">Loss Time by Reason</h1>
                    <h1 className="container_8__text-detail">
                    รายงานสถิติสาเหตุที่ทำให้เครื่องจักรหยุดทำงานรูปแบบกราฟ เรียงลำดับสาเหตุที่เกิดขึ้นสูงสุด ไปยังน้อยสุด พร้อมสรุประยะเวลารวมที่เกิดสาเหตุนั้นๆ
                    </h1>
                    <div style={{width:50,height:"1vh"}}/>
                    <h1 className="container_8__text-note" >** หมายเหตุ ** ทุกรูปแบบของ Report สามารถเลือก ช่วงระยะเวลาเป็นรายวัน หลายวัน สัปดาห์ เดือน หรือรวมทั้งหมดตั้งแต่ติดตั้งระบบ และยังสามารถเลือกเฉพาะกลุ่มของเครื่องจักร หรือเลือกเฉพาะเครื่องจักรใด เครื่องหนึ่งได้</h1>
                  </div>
                </Fade>
              </div>
            </div>
          </Sticky>

          <Sticky top={9} enabled={true}>
            <div  className="container_9 ">
              <Fade down  duration={2500} >
                <div className="dummy" /> 
                <h1 className="container_9_text-header" >เพียงมี Towerlight ก็สามารถติดตั้งได้อย่างง่ายดาย</h1>
              </Fade>
              <div className="container_9_text" >
                <div>
                  <img  src={Towerlight_text} className="container_9_picture" alt=""/>
                </div>
                <div className="container_9__text-detail">
                  <p >
                    Tower light รุ่นมาตรฐาน คือ LR5-302pjbw-ryg หรือ LR6-302pjbw-ryg สามารถรับสัญญาณได้ 8 สัญญาณ จะมาพร้อมไฟ LED สีแดง เขียว เหลือง สีน้ำเงิน กับสีขาวจะต้องซื้อแยกต่างหาก สัญญาณนับชิ้นงาน สามารถต่อสายสัญญาณกับ Tower light สีมาตรฐานได้เลย โดยไม่จำเป็นต้องซื้อไฟ LED สีน้ำเงินเพิ่ม รวมถึงสีขาวเช่นกัน สามารถเขื่อมต่อสายสัญญาณนับของเสียได้ โดยไม่จำเป็นต้องซื้อไฟ LED สีขาวเพิ่ม
                  </p>
                </div>
              </div>
            </div>
          </Sticky>

          <Sticky top={8} enabled={true}>
            <div className="container_10" >
              {/* <Fade down duration={2500} > */}
                <h1 className="container_10__text">ติดตั้งง่าย ไร้สาย<br/>ไม่ขึ้นกับยี่ห้อของเครื่องจักรหรือแบรนด์ใดๆ</h1>
              {/* </Fade> */}
              <video className="container__10__video" autoPlay={true} loop={true} muted={isVideoMuted3} src={Install_Towerlight} />
              {/* {safari && <button onClick={handleMuteState3} className="button_safari_10">{isVideoMuted3 ? 'Play' : 'Puase'}</button >} */}
            </div>
          </Sticky>

          <Sticky top={9} enabled={true} >
            <div className="container__11"  >
              <Fade up  duration={2500} >
                <h4 className="container__11_text-header">ตอบโจทย์การรองรับเซิร์ฟเวอร์ของคุณ</h4>
              </Fade>
              <div className="container__11_2">
                <Fade left  duration={2500} >
                  <div>
                    <img className="container__11_picture" src={Server_OnPermise} alt=""/>
                    <h4 className="container__11_text-right">ON PERMISE</h4>
                  </div>
                </Fade>   
                <Fade right  duration={2500} >
                  <div>
                    <img className="container__11_picture" src={Server_aws} alt=""/>
                    <h4 className="container__11_text-right">ON CLOUD</h4>
                  </div>
                </Fade>
              </div>
            </div>
          </Sticky>
        </div>
    </div>
  );
}

export default Home2;
